# Laravel 5.0 project
This is a part of more complex project with 2 different Laravel installations and 3 external libraries: core, API and database library (on client's request).
It is connected to other parts with API gateway.

> The aim of the project is to create a “search process” with
> different parameters, selecting social media sources, like
> Instagram, Twitter, etc., and then running the process, ie:
> selecting from both sources images that are relevant to the tag selected,
> or by geolocation if you choose this option.

### Selecting images and process them
After images are selected (max 20 from each source), there is first validation to exclude duplicated images (not always possible, as images have different URLs, or name, …) and then saving them locally and displaying them to the user.

Afterwards, the user can edit images, and save them to the server over API.
User can run process many times, each time preselected images are removed (if the project run again in the same session), ie: there is a pagination process that is running underground.

##### Images are sent to the second laravel installation, inserted into the database

