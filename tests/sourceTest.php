<?php

use Tesis\Database;
use Tesis\Photos\Core\Tables\Source;

class sourceTest extends PHPUnit_Framework_TestCase {

    public $classRepo;
    public $source;
    public $dbName;
    public $table;

    public function setUp()
    {
        $this->table = 'source';
        $this->dbName = 'phlow_source';

        $this->classRepo = 'Tesis\Photos\Core\Tables\Source';

        $this->data = ['table'=> $this->table, 'id' => '3', 'name' => 'getty'];
        //class has to be initialized before any tests
        $this->source = new Source($this->data);
    }

    public function tearDown()
    {
        //
    }

    /**
     * test_If_Variables_for_DB_AND_Tables_Defined
     *
     * @param $a variable to test
     * @param $expected the class we expected to be in
     *
     * @dataProvider variablesDBProvider
     *
    */
    public function test_If_Variables_for_User_Defined($a, $expected)
    {
        $actual = $this->classRepo;

        $this->assertClassHasAttribute($a, $actual, 'Expected Pass');
    }
    /**
    *
    * variablesDBProvider
    *
    * a provider for test_If_Variables_for_DB_AND_Tables_Defined
    *
    */
    public function variablesDBProvider()
    {
        return array(
            array('table', $this->classRepo, 'Expected Pass'),
            array('tablePK', $this->classRepo, 'Expected Pass'),
            array('dbFields', $this->classRepo, 'Expected Pass'),
            array('required', $this->classRepo, 'Expected Pass'),
        );
    }
    /**
     * test_Get_By_Id_Pass
     *
    */
    public function test_Get_By_Id_Pass()
    {
        $test = $this->source;
        $id = 1;
        $test->where(['id'=>$id])->first();

        $result = $test->fetch();
        $this->assertNotEmpty($result, 'Expected Pass');
    }
    /**
     * test_Get_By_Id_Fail
     *
    */
    public function test_Get_By_Id_Fail()
    {
        $test = $this->source;
        $id = 14;
        $test->where(['id'=>$id])->first();

        $result = $test->fetch();

        $this->assertEmpty($result, 'Expected Fail');
    }

}
