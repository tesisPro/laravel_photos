<?php
use Tesis\Database;
use Tesis\Photos\Core\Tables\Photo;


//class photoTest extends TestCase
class photo
{
    public $classRepo;
    public $source;
    public $dbName;
    public $table;

    public function setUp()
    {
        parent::setUp();
        $this->table = 'photos';
        $this->dbName = 'tesis_apis';

        $this->classRepo = 'Tesis\Api\Photo';

        $this->data = ['name' => 'getty'];
        //class has to be initialized before any tests
        $this->photo = new Photo($this->data);
    }

    public function tearDown()
    {
        //
    }
    public function test_create_Pass()
    {
        $data = ['name' => 'getty'];
        $create = $this->photo->create($data);

        $this->assertNotEmpty($create, 'Expected Pass');
        $this->assertGreaterThan(0, $create);

    }
    public function test_create_empty_id_Pass()
    {
        $data = ['id' => '', 'name' => 'getty'];
        $create = $this->photo->create($data);

        $this->assertNotEmpty($create, 'Expected Pass');
        $this->assertGreaterThan(0, $create);
    }
    /**
     * test_create_same_id_Fail
     *
     * @expectedException PDOException
     *
    */
    public function test_create_same_id_Fail()
    {
        $data = ['id' => '3', 'name' => 'getty'];
        $create = $this->photo->create($data);
    }
}
