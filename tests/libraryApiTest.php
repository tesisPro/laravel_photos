<?php

use Tesis\Database;
use Tesis\Photos\Core\Library;
use Tesis\Photos\Core\Traits\LoaderTrait;
use Tesis\Photos\Core\Helpers\TesisException;

use Tesis\Photos\Core\Tables\Photo;
//test with API database
class LibraryApiTest extends PHPUnit_Framework_TestCase
{
    use LoaderTrait;

    public $classRepo;
    public $process;

    public function setUp()
    {
        parent::setUp();
        $this->classRepo = 'Tesis\Photos\Core\Library';
        $this->library = new Library('configApi.ini');

    }
    public function tearDown()
    {
        //
    }


    public function test_photos_find_all_Pass()
    {
        $photos = $this->library->photos()->findAll();

        $this->assertNotEmpty($photos, 'Expected Pass');
    }
    public function test_photos_find_by_tag_Pass()
    {
        $photos = $this->library->photos()->findByTag(45);

        $this->assertNotEmpty($photos, 'Expected Pass');
    }
    public function test_photos_find_by_location_Pass()
    {
        /*$latitude = 48;
        $longitude = 2;
        $distance = 500;
        $query = 'SELECT id, name, latitude, longitude, ( 6371 * acos( cos( radians(48.851418) )
* cos( radians( latitude ) ) * cos( radians( longitude ) - radians(2.396511) ) + sin( radians(48.851418
) ) * sin( radians( latitude ) ) ) ) AS distance FROM tesis_apis.photos HAVING distance < 500';

        $query = 'SELECT id, name, latitude, longitude, ( 6371 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance FROM tesis_apis.photos HAVING distance < ' . $distance;
        $this->photo = new Photo;
        $photo = $this->photo->exec($query);
        $photos = $photo->fetchAll();*/
        $photos = $this->library->photos()->findByLocation(48,2,1000);

        $this->assertNotEmpty($photos, 'Expected Pass');
    }
}
