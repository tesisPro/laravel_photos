<?php

use Tesis\Database;
use Tesis\Photos\Core\Tables\Process;

class ProcessTest extends PHPUnit_Framework_TestCase {

    public $classRepo;
    public $process;
    public $dbName;
    public $table;

    public function setUp()
    {
        $this->table = 'process';
        $this->dbName = 'phlow_source';

        $this->classRepo = 'Tesis\Photos\Core\Tables\Process';

        $this->data = ['table'=> $this->table, 'userId' => '3', 'tag' => 'dog'];
        //class has to be initialized before any tests
        $this->process = new Process($this->data);
    }

    public function tearDown()
    {
        //
    }

    /**
     * test_If_Variables_for_DB_AND_Tables_Defined
     *
     * @param $a variable to test
     * @param $expected the class we expected to be in
     *
     * @dataProvider variablesDBProvider
     *
    */
    public function test_If_Variables_for_User_Defined($a, $expected)
    {
        $actual = $this->classRepo;

        $this->assertClassHasAttribute($a, $actual, 'Expected Pass');
    }
    /**
    *
    * variablesDBProvider
    *
    * a provider for test_If_Variables_for_DB_AND_Tables_Defined
    *
    */
    public function variablesDBProvider()
    {
        return array(
            array('table', $this->classRepo, 'Expected Pass'),
            array('tablePK', $this->classRepo, 'Expected Pass'),
            array('dbFields', $this->classRepo, 'Expected Pass'),
            array('required', $this->classRepo, 'Expected Pass'),
        );
    }
    /**
    *
    * test_CheckInput_Expected_Pass
    *
    */
    public function test_CheckInput_Expected_Pass()
    {
        $fields = 'userId, tag';

        $result = $this->process->checkInput($fields);
        $this->assertSame('userId,tag', $result, 'Expected Pass');
    }
    /**
    *
    * testCheckInput_If_We_Expect_Space_After_Comma_Expected_Fail
    */
    public function testCheckInput_If_We_Expect_Space_After_Comma_Expected_Fail()
    {
        $fields = 'userId, tag';

        $result = $this->process->checkInput($fields);
        $this->assertNotSame('uid, username', $result, 'Expected Fail');
    }
    /**
     * test_Get_By_Id_Pass
     *
     * @exceptionExpected
     *
    */
    public function test_Get_By_Id_Pass()
    {
        $test = $this->process;
        $id = 1;
        $test->where(['id'=>$id])->first();

        $result = $test->fetch();
        $this->assertNotEmpty($result, 'Expected Pass');
    }
    /**
     * test_Get_Process_By_Params_WhereOr
     *
     * @exceptionExpected
     *
    */
    public function test_Get_Process_By_Params_WhereOr()
    {
        $params = ['cron'=>'','userId' => 5];
        $process = new Process();

        $process->whereOr($params)->first();

        $result = $process->fetch();
        //print_r($result);
        $this->assertNotEmpty($result, 'Expected Pass');
    }
    /**
     * test_Get_Process_By_Params_Where
     *
     * @exceptionExpected
     *
    */
    public function test_Get_Process_By_Params_Where()
    {
        $params = ['cron'=>'','userId' => 5];
        $process = new Process();

        $process->where($params)->first();

        $result = $process->fetch();
        $this->assertNotEmpty($result, 'Expected Pass');
    }


}
