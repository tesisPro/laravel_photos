<?php

use Tesis\Database;
use Tesis\Photos\Core\Library;
use Tesis\Photos\Core\Tables\Process;
use Tesis\Photos\Core\Tables\ProcessSource;


//class ProcessSourcesStoryTest extends PHPUnit_Framework_TestCase {
class ProcessSourcesStory {

    public $process;
    public $processSources;
    public $dbName;
    public $table;

    public function setUp()
    {
        $this->tableProcess = 'process';
        $this->tableProcessSource = 'processSource';
        $this->dbName = 'phlow_source';

        $this->dataProcess = ['table'=> $this->table, 'userId' => '3', 'tag' => 'dog'];
        $this->dataProcessStory = ['table'=> $this->table, 'userId' => '3', 'tag' => 'dog'];
        //class has to be initialized before any tests
        $this->library = new Library;
        $this->process = new Process();
        $this->processSource = new ProcessSource();
    }

    public function tearDown()
    {
        //
    }

    /**
     * countProcess count process to make comparisions
     * helper test
     *
    */
    public function test_countProcess_Expected_Pass()
    {
        $process = new Process;
        $select = $process->select('id')->all();
        $res = $process->fetch();
        $count = sizeof($res);

        $this->assertNotEquals(0, 'Expected Pass');
        return $count;
    }
    /**
     * test_Create_Process_Pass
     *
     * dependency test: create, then update and delete
     * return lastId
     *
    */
    public function test_Create_Process_Pass()
    {
        $data = ['table'=> $this->table, 'name' => 'tesi'.time(), 'tag' => time().'tag'];

        $processId = $this->process->create($data);

        $count = $this->test_countProcess_Expected_Pass();

        $this->assertGreaterThanOrEqual($count, $processId, 'Expected Pass');

        return $processId;
    }

    public function processSourceExists($processId='')
    {
        if(empty($processId))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }

        $this->processSource->where(['processId'=>$processId])->all();
        $processSource = $this->processSource->fetch();

        return !empty($processSource) ? $processSource : false;
    }


    public function actionProcessSources(array $sources=null, $processId, $action='create')
    {
        if(is_null($sources))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }
        foreach($sources as $sourceId)
        {
            $data = ['table'=>$this->tableProcessSource, 'processId' => $processId, 'sourceId'=> $sourceId];
            if($action == 'create')
            {
                $updated = $this->processSource->create($data);
            }
            if($action == 'delete')
            {
                $updated = $this->processSource->delete($data);
            }
        }
    }


    /**
     * test_createProcessSource
     *
     * @depends test_Create_Process_Pass
     *
    */
    public function test_createProcessSource($processId, array $sources = null)
    {
        $sources = [1,4];
        //check if sources not null, processId not null checked before calling this method
        if(empty($processId))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }
        //2-check if record exists in processSources
        $processSource = $this->processSourceExists($processId);

        //-- when editing process
        if(!empty($processSource))
        {
            $sourcesExist = []; //build sources existed
            $sourcesDelete = []; //build sources to delete
            foreach($processSource as $key=>$value)
            {
                array_push($sourcesExist, $value['sourceId']);
            }
            //compare arrays, take further actions: insert/update
            $insert = array_diff($sources, $sourcesExist);
            $delete = array_diff($sourcesExist, $sources);

            if(!empty($insert))
            {
                //insert record
                //echo 'insert !!! record: ' . print_r($insert);
                $insert = $this->actionProcessSources($insert, $processId,'create');
            }
            if(!empty($delete))
            {
                //delete record
                //echo 'delete record ' . print_r($delete);
                $delete = $this->actionProcessSources($delete, $processId,'delete');
            }
        }
        //when inserting new process
        else
        {
            //safe to insert
            $insert = $this->actionProcessSources($sources, $processId,'create');
        }
    }
    /**
     * test_Update_Process_Pass
     *
     * @depends test_Create_Process_Pass
     *
    */
    public function test_Update_Process_Pass($processId='')
    {
        $sources = [2,4];
        //check if sources not null, processId not null checked before calling this method
        if(empty($processId))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }
        //2-check if record exists in processSources
        $processSource = $this->processSourceExists($processId);

        //-- when editing process
        if(!empty($processSource))
        {
            $sourcesExist = []; //build sources existed
            $sourcesDelete = []; //build sources to delete
            foreach($processSource as $key=>$value)
            {
                array_push($sourcesExist, $value['sourceId']);
            }
            //compare arrays, take further actions: insert/update
            $insert = array_diff($sources, $sourcesExist);
            $delete = array_diff($sourcesExist, $sources);

            if(!empty($insert))
            {
                //insert record
                //echo 'insert !!! record: ' . print_r($insert);
                $insert = $this->actionProcessSources($insert, $processId,'create');
            }
            if(!empty($delete))
            {
                //delete record
                //echo 'delete record ' . print_r($delete);
                $delete = $this->actionProcessSources($delete, $processId,'delete');
            }
        }

    }
    /**
     * test_Delete_Process_Pass
     *
     * @depends test_Create_Process_Pass
     *
    */
    public function test_Delete_Process_Pass($processId='')
    {
        //first delete records from processSources
        //check if sources not null, processId not null checked before calling this method
        if(empty($processId))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }

        //2-check if record exists in processSources
        $processSource = $this->processSourceExists($processId);
        if(!$processSource)
        {
            throw new \Exception(NO_RECORDS);
            //nothing to delete from sources table
        }
        if(!empty($processSource))
        {
            $delete = []; //build sources to delete, ie: all occurences
            foreach($processSource as $key=>$value)
            {
                array_push($delete, $value['sourceId']);
            }

            if(!empty($delete))
            {
                //delete record
                echo 'delete record ' . print_r($delete);
                $delete = $this->actionProcessSources($delete, $processId,'delete');
            }
        }
        //3-check if process exists
        $find = $this->process->where(['id'=>$processId])->all();
        $found = $this->process->fetch();
        if(!$found)
        {
            throw new \Exception(NO_RECORDS);
            //nothing to delete from process table
        }

        $this->assertNotEmpty($processSource, 'Expected Pass - processSource exists');
        $this->assertNotEmpty($found, 'Expected Pass - process exists');

        $deleteProcess = $this->process->delete($processId);
        $this->assertEquals(1, $deleteProcess, 'Expected pass: process is deleted');
        //echo 'process deleted: ' . $processId;

    }
    /**
     * test_Delete_Process_EmptyArgs_Fail
     *
     * @expectedException     \Exception
     *
    */
    public function test_Delete_Process_EmptyArgs_Fail($processId='')
    {
        if(empty($processId))
        {
            throw new \Exception(MISSING_ARGUMENTS);
        }
    }
    /**
    * test_Delete_Missing_ID_Expected_Fail
    *
    */
    public function test_Delete_Missing_Args_Expected_Fail()
    {
        $test = $this->process;

        $result = $test->delete();

        $this->assertSame(false, $result, 'Expected fail');
    }
}
