<?php

use Tesis\Database;
use Tesis\Photos\Core\Tables\ProcessImages;

class ProcessImagesTest extends PHPUnit_Framework_TestCase {

    public $classRepo;
    public $processImages;
    public $dbName;
    public $table;

    public function setUp()
    {
        $this->table = 'processSource';
        $this->dbName = 'phlow_source';

        $this->classRepo = 'Tesis\Photos\Core\Tables\ProcessImages';

        $this->data = ['table'=> $this->table, 'processId' => '3', 'sourceId' => '1'];
        //class has to be initialized before any tests
        $this->processImages = new ProcessImages($this->data);
    }

    public function tearDown()
    {
        //
    }

    /**
     * test_If_Variables_for_DB_AND_Tables_Defined
     *
     * @param $a variable to test
     * @param $expected the class we expected to be in
     *
     * @dataProvider variablesDBProvider
     *
    */
    public function test_If_Variables_for_User_Defined($a, $expected)
    {
        $actual = $this->classRepo;

        $this->assertClassHasAttribute($a, $actual, 'Expected Pass');
    }
    /**
    *
    * variablesDBProvider
    *
    * a provider for test_If_Variables_for_DB_AND_Tables_Defined
    *
    */
    public function variablesDBProvider()
    {
        return array(
            array('table', $this->classRepo, 'Expected Pass'),
            array('tablePK', $this->classRepo, 'Expected Pass'),
            array('dbFields', $this->classRepo, 'Expected Pass'),
            array('required', $this->classRepo, 'Expected Pass'),
        );
    }
    /**
     * test_Get_By_processId_Pass
     *
    */
    public function test_Get_By_processId_Pass()
    {
        $test = $this->processImages;
        $processId = 1;
        $test->where(['processId'=>$processId])->all();

        $result = $test->fetch();
        $this->assertNotEmpty($result, 'Expected Pass');
    }
    /**
     * test_Get_By_sourceId_Pass
     *
    */
    public function test_Get_By_sourceId_Pass()
    {
        $test = $this->processImages;
        $sourceId = 1;
        $test->where(['sourceId'=>$sourceId])->all();

        $result = $test->fetch();
        $this->assertNotEmpty($result, 'Expected Pass');
    }

}
