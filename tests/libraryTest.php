<?php

use Tesis\Database;
use Tesis\Photos\Core\Library;
use Tesis\Photos\Core\Traits\LoaderTrait;
use Tesis\Photos\Core\Helpers\TesisException;

//tests with default DB
class LibraryTest extends PHPUnit_Framework_TestCase
{
    use LoaderTrait;

    public $classRepo;
    public $process;

    public function setUp()
    {
        parent::setUp();
        $this->classRepo = 'Tesis\Photos\Core\Library';
        $this->library = new Library('config.ini');

    }
    public function tearDown()
    {
        //
    }

    /**
     * initial test to see if roots are working
     *
    */
    public function testProcess_If_Class_Has_Id_Variable()
    {
        $this->assertClassHasAttribute('process', $this->classRepo, 'Expected Pass');
        $this->assertClassHasAttribute('source', $this->classRepo, 'Expected Pass');
        $this->assertClassHasAttribute('processSource', $this->classRepo, 'Expected Pass');
    }
    public function test_if_process_loads()
    {
        $process = $this->library->process();
        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    public function test_process_find_Pass()
    {
        $params = ['cron'=>'','userId' => 5];
        $process = $this->library->process()->find($params);

        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    public function test_process_find_by_id_Pass()
    {
        $params = ['id'=>'3'];
        $process = $this->library->process()->find($params);

        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    public function test_process_find_all_by_user_Pass()
    {
        $params = ['userId'=>'3'];
        $process = $this->library->process()->findAll($params);

        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    public function test_process_find_all_by_type_manual_Pass()
    {
        $params = ['cron'=>''];
        $process = $this->library->process()->findAll($params);

        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    public function test_sources_find_all_Pass()
    {
        $params = [];
        $process = $this->library->source()->findAll($params);

        $this->assertTrue(!empty($process), 'Expected Pass');
    }
    /**
     * test_process_create_Pass
     *
    */
    public function process_create_Pass()
    {
        $data = ['name' => 'tesi'.time(), 'tag' => time().'tag'];

        $processId = $this->library->process()->create($data);
        $this->assertTrue(!empty($processId), 'Expected Pass');
        $sources = [1,4];
        //2-check if record exists in processSources
        $processSource = $this->library->processSource()->action($sources, $processId, 'create');
        $this->assertTrue(true, 'Expected Pass');
    }
    /**
     * test_process_create_Fail
     *
     * @expectedException Tesis\Photos\Core\Helpers\TesisException
     *
    */
    public function test_process_create_Fail()
    {
        $data = ['name' => 'tesi'.time(), 'tag' => time().'tag'];

        $sources = [1,4];
        if(empty($processId))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }
        //2-check if record exists in processSources
        $processSource = $this->library->processSource()->action($sources, $processId, 'create');

    }
    /**
     * test_process_update_Pass
     *
    */
    public function test_process_update_Pass()
    {
        $processId = 10;
        $params = ['id'=>$processId, 'tag'=>'dogs'];
        $process = $this->library->process()->update($params);

        $sources = [1,2,3];

        $processSource = $this->library->processSource()->update($processId, $sources);

    }
    /**
     * test_delete_process_Pass
     *
    */
    public function delete_process_Pass()
    {
        $processId = 69;

        //2-check if record exists in processSources
        $processSource = $this->library->processSource()->exists($processId);
        if(!$processSource)
        {
            throw new TesisException(NO_RECORDS);
            //nothing to delete from sources table
        }
        if(!empty($processSource))
        {
            $delete = []; //build sources to delete, ie: all occurences
            foreach($processSource as $key=>$value)
            {
                array_push($delete, $value['sourceId']);
            }
            if(!empty($delete))
                $this->library->processSource()->action($delete, $processId,'delete');
        }
        //3-check if process exists
        $params = ['id'=> $processId];
        $process = $this->library->process()->find($params);
        if(!$process)
        {
            throw new \Exception(NO_RECORDS);
            //nothing to delete from process table
        }

        $this->assertNotEmpty($processSource, 'Expected Pass - processSource exists');
        $this->assertNotEmpty($process, 'Expected Pass - process exists');

        $deleteProcess = $this->library->process()->delete($processId);
        $this->assertEquals(1, $deleteProcess, 'Expected pass: process is deleted');
        //echo 'process deleted: ' . $processId;

    }

}
