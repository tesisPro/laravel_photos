<?php

namespace Tesis\Photos\Core\Faces;

interface CrudInterface {

    public function create(array $params = null);
    public function update(array $params = null);
    public function delete($id='');
    public function exists($id='');
}
