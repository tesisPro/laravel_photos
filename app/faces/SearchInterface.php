<?php
namespace Tesis\Photos\Core\Faces;

interface SearchInterface {

    public function find(array $params = null);
    public function findAll(array $params = null);
}
