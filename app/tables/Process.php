<?php //tables/Process.php


namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;
use Tesis\Photos\Core\Traits\ObjectTrait as NewObjectTrait;

class Process extends DataObject
{
    use NewObjectTrait;
    /**
     * @access protected
     * @var string
     */
    public $table = 'process';

    /**
     * @access protected
     * @var string
     */
    public $tablePK = 'id';
    /**
     * @access public
     * @var array
     */
    public $dbFields = ['id', 'userId', 'latitude', 'longitude', 'minQualityScore', 'cron', 'tag', 'name', 'deleted', 'deleter'];
    /**
     * @access public
     * @var array
     */
    public $required = ['tag', 'name'];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);
        //create an object and assign values if exists
        $this->createObject($dataArray);
    }

}
