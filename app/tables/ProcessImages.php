<?php //tables/ProcessImages.php


namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;

class ProcessImages extends DataObject
{
    /**
     * @access protected
     * @var string
     */
    public $table = 'processedImages';

    /**
     * @access protected
     * @var string
     */
    public $tablePK = 'id';
    /**
     * @access public
     * @var array
     */
    public $dbFields = ['sourceId', 'processId', 'uniqueIdentifier', 'imported', 'deleted'];
    /**
     * @access public
     * @var array
     */
    public $required = ['processId', 'sourceId', 'uniqueIdentifier'];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);

        $this->date = date('Y-m-d H:i:s');

    }

}
