<?php //app/tables/Photo.php

namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;
use Tesis\Photos\Core\Traits\ObjectTrait as NewObjectTrait;
use Tesis\Photos\Core\Traits\EncryptionTrait;
use Tesis\Photos\Core\Traits\CurlTrait;

class Photo extends DataObject
{
    use NewObjectTrait, EncryptionTrait, CurlTrait;

    /**
     * @access protected
     * @var string
     */
    public $table = 'photos';

    /**
     * @access protected
     * @var string
     */
    public $tablePK = 'id';
    /**
     * @access public
     * @var array
     */
    //geolocation, exif
    public $dbFields = ['id', 'userId', 'name', 'description', 'latitude', 'longitude', 'isPublished', 'created', 'updated'];
    /**
     * @access public
     * @var array
     */
    public $required = [];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);
        //create an object and assign values if exists
        $this->createObject($dataArray);

    }


}
