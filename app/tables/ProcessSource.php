<?php //tables/ProcessSource.php


namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;

class ProcessSource extends DataObject
{
    /**
     * @access protected
     * @var string
     */
    public $table = 'processSource';

    /**
     * @access protected
     * @var array
     */
    public $tablePK = ['processId', 'sourceId'];
    /**
     * @access public
     * @var array
     */
    public $dbFields = ['processId', 'sourceId'];
    /**
     * @access public
     * @var array
     */
    public $required = ['processId', 'sourceId'];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);

        $this->date = date('Y-m-d H:i:s');

    }

}
