<?php //app/tables/PhotoTag.php

namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;
use Tesis\Photos\Core\Traits\ObjectTrait as NewObjectTrait;

class PhotoTag extends DataObject
{
    use NewObjectTrait;

    /**
     * @access protected
     * @var string
     */
    public $table = 'photoTags';

    /**
     * @access protected
     * @var string
     */
    public $tablePK = 'id';
    /**
     * @access public
     * @var array
     */
    //geolocation, exif
    public $dbFields = ['tagId', 'photoId'];
    /**
     * @access public
     * @var array
     */
    public $required = [];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);
        //create an object and assign values if exists
        $this->createObject($dataArray);

    }


}
