<?php //tables/Source.php


namespace Tesis\Photos\Core\Tables;

use Tesis\Database\PDORepository as DataObject;

class Source extends DataObject
{
    /**
     * @access protected
     * @var string
     */
    public $table = 'source';

    /**
     * @access protected
     * @var string
     */
    public $tablePK = 'id';
    /**
     * @access public
     * @var array
     */
    public $dbFields = ['name', 'isActive', 'consumerKey', 'consumerSecret', 'oauthAccessToken', 'oauthAccessTokenSecret', 'screenName'];
    /**
     * @access public
     * @var array
     */
    public $required = ['name', 'isActive'];

    /**
     * __construct
     *
     * @param array $dataArray an array passed to the object
     *
     * @return none
     *
     * @access public
     *
     *
     */
    public function __construct(array $dataArray = null)
    {
        parent::__construct($dataArray);

        $this->date = date('Y-m-d H:i:s');

    }

}
