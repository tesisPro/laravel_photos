<?php
namespace Tesis\Photos\Core\Helpers;

/*
config.ini file should be in root
params.php includes array of defined constants

const.php should be included
usage:
$config = Config::load('/app/config/test.php');
$timezone = $config->get('timezone');


$config = Loader::load('app/config/test.php');
$configIni = Loader::loadIni('config.ini');
echo $timezone = $config->get('timezone');
print_r($config->parseIni());

*/
class Loader
{
    public $settings;

    public function __construct($settings = array())
    {
        $this->settings = $settings;
    }

    public static function load($path)
    {
        $config = new static();

        if (file_exists($path))
            return new static(include_once $path);
        else{
            throw new \Exception ('Missing cofig file: ' . $path);
        }
        return new static;
    }
    /**
     * loadIni load ini file, parse it, define constants for DB access
     *         add debug options (development environment displays errors,
     *         others only catch them in error.log file (root)
     *
     * @param string $path path to ini file
     *
     * @return
     *
    */
    public function loadIni($path)
    {
        $config = new self();

        if (file_exists($path))
        {
            $ini_array = parse_ini_file($path, true);
            extract($ini_array);

            foreach($ini_array['database'] as $key => $val)
            {
                $config->set($key, $val);
            }
            foreach($ini_array['general'] as $k => $v)
            {
                $config->set($k, $v);

                if(strtolower($k) == 'environment' && strtolower($v) == 'development')
                {
                    ini_set('display_errors', '1');
                }
                else
                {
                    ini_set('display_errors', '0');
                }
            }
        }
        else
        {
            throw new \Exception ('Missing config ini file' . $path);
        }
        //print_r($config);
        return $config;
    }
    public function get($key, $default = null)
    {
        return isset($this->settings[$key]) ? $this->settings[$key] : $default;
    }

    public function set($key, $value)
    {
        $this->settings[$key] = $value;
    }
}
