<?php

namespace Tesis\Photos\Core;

use Tesis\Database;
use Tesis\Photos\Core\Helpers\TesisException;
use Tesis\Photos\Core\Controllers\ProcessController;
use Tesis\Photos\Core\Controllers\SourceController;
use Tesis\Photos\Core\Controllers\ProcessSourceController;
use Tesis\Photos\Core\Controllers\ProcessImagesController;
use Tesis\Photos\Core\Traits\LoaderTrait;
use Tesis\Photos\Core\Traits\CurlTrait;
use Tesis\Photos\Core\Traits\LoadTrait;
use Tesis\Photos\Core\Traits\EncryptionTrait;
use Tesis\Api\Models\User;
use Tesis\Photos\Core\Tables\Photo;
use Tesis\Photos\Core\Tables\Tag;
use Tesis\Photos\Core\Tables\PhotoTag;

use Tesis\Photos\Core\Controllers\PhotoController;
use Tesis\Photos\Core\Controllers\TagController;
use Tesis\Photos\Core\Controllers\PhotoTagController;
use Tesis\Api\Library as APILibrary;

/*if($_SERVER['SERVER_ADDR'] == '127.0.0.1'){
	error_reporting(E_ALL);
	ini_set('default_charset', 'utf-8');
	ini_set('error_log','/var/www/tesis.photos.core/error.log');
	ini_set('display_errors', '1');
}*/

require_once dirname(__DIR__).'/vendor/autoload.php';

class Library
{
    use LoaderTrait,CurlTrait, EncryptionTrait;

    public $process;
    public $source;
    public $processSource;
    public $userEndpoint;
    public $publicKey;
    public $privateKey;

    public function __construct($configFile='')
    {
        $this->encryptionTimestamp = time();

        if(empty($configFile))
            $configFile = dirname(__FILE__).'/config.ini';

        $this->configIni = $this->loader($configFile);
    }
    public function process()
    {
        return new ProcessController;
    }
    public function source()
    {
        return new SourceController;
    }
    public function processSource()
    {
        return new ProcessSourceController;
    }
    public function photos()
    {
        return new PhotoController;
    }
    /**
     * processImages
     *
     * @param array $params all necessary parameters needed for compare/insert/push proces
     *
     * @return
     *
    */
    public function processImages()
    {
        return new ProcessImagesController;
    }
    public function pushImages(array $params=null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
            return false;
        }
        //passed parameters
        $arrayRequired = ['processId','sourceId','photoId','photoSrc','tags','publicToken', 'privateToken'];

        foreach ( $arrayRequired as $key => $value )
        {
			if ( !in_array( $key, $params ) && empty($value))
            {
                throw new TesisException(MISSING_ARGUMENTS . $key);
				return false;
			}
		}

        $controller = new ProcessImagesController;

        try{
            $insert = $controller->insertImages($params);
            if($insert)
            {
                $apiPhotos = new Photo($params);

                $apiPhotos->signClass($params['publicToken'], $params['privateToken']);
                $data = $apiPhotos->buildParams($this->publicKey, $this->privateKey);
                $results = $this->goCurl($this->photosEndpoint, $data, true);
            }
        } catch(TesisException $e){
            $e->getMessage();
        }

    }
    /**
     * getUser fetching user data through API
     *
     * @param string $publicToken
     * @param string $privateToken
     *
     * @return object
     *
    */
    public function fetchUserDetails($publicToken, $privateToken)
    {
        $apiUser = new User();

        $apiUser->signClass($publicToken, $privateToken);
        $data = $apiUser->buildParams($this->publicKey, $this->privateKey);
        $results = $this->goCurl($this->userEndpoint, $data, false);

        return $results;
    }

}
