<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\ProcessImages;
use Tesis\Photos\Core\Faces\SearchInterface;
use Tesis\Photos\Core\Faces\CrudInterface;
use Tesis\Photos\Core\Helpers\TesisException;

class ProcessImagesController implements SearchInterface, CrudInterface
{
    //endpoint defined in config.ini

    /**
     * __construct initialize process and processImages
     *
     *
    */
    public function __construct()
    {
        $this->processImages = new ProcessImages;
    }
    /**
     * find single process, append sources for this process
     *
     * @param array $params parameters can be empty or null
     * @param bool  $findSources if set to true, search sources as well
     *                           otherwise only projects
     *
     * @return object
     *
    */
    public function find(array $params = null, $findSources = true)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->processImages->where($params)->first();

        $process = $this->processImages->fetchOne();

        //find sources
        if(empty($process))
        {
            throw new TesisException(NO_RECORDS);
        }

        return $process ? $process : false;
    }
    /**
     * findAll find all processes, append sources for each process
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            $this->processImages->select('*')->all();
        }
        else
        {
            $this->processImages->where($params)->all();
        }

        $processes = $this->processImages->fetch();

        if(!$processes)
        {
            throw new TesisException(NO_RECORDS);
        }

        return $processes ? $processes : false;
    }
    /**
     * create
     *
     * @param array $params parameters needed to create new process
     *
     * @return int lastInsertId
     *
    */
    public function create(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $create = $this->processImages->create($params);
        return $create;
    }
    /**
     * update
     *
     * @param array $params parameters we'd like to update
     *              id is required
     *
     * @return object/bool
     *
    */
    public function update(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $update = $this->processImages->update($params);

        return $update;
    }
    /**
     * delete
     *
     * @param int $id id of the process
     *
     * @return object/bool
     *
    */
    public function delete($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $delete = $this->processImages->delete($id);

        return $delete ? $delete : false;
    }
    /**
     * exists check if process exists
     *
     * @param int $id id of the process
     *
     * @return object/bool
     *
    */
    public function exists($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->processImages->where(['id'=>$id])->first();

        $result = $this->processImages->fetch();

        return !empty($result) ? $result : false;
    }
    /**
     * pushImages check if it exists in DB, if  not insert and push to the API db
     *
     * @param array $params
     * required params:
     * $array = ['processId','sourceId','photoId','photoSrc','tags',
     * 'text','latitude','longitude','publicToken', 'privateToken'];
     *
     * @return
     *
    */
    public function insertImages(array $params = NULL)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
            return false;
        }

        //insert record - compare if exist and then push
        $params = ['sourceId'=>$params['sourceId'], 'processId'=>$params['processId'], 'uniqueIdentifier'=>$params['photoId']];

        try{
            $findRecord = $this->findAll($params);
        } catch(TesisException $e){
            if($e->getMessage() == 'No records found'){
                $insert = $this->create($params);
                return $insert;
            }
        }

    }
}
