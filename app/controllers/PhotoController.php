<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\Photo;
use Tesis\Photos\Core\Controllers\PhotoTagController;
use Tesis\Photos\Core\Faces\SearchInterface;
use Tesis\Photos\Core\Faces\CrudInterface;
use Tesis\Photos\Core\Helpers\TesisException;

class PhotoController implements SearchInterface, CrudInterface
{

    /**
     * __construct initialize photo and photoTag
     *
     *
    */
    public function __construct()
    {
        $this->photo = new Photo;

        $this->photoTagController = new PhotoTagController;
    }
    /**
     * find single photo, append tags for this photo
     *
     * @param array $params parameters can be empty or null
     * @param bool  $findSources if set to true, search tags as well
     *                           otherwise only projects
     *
     * @return object
     *
    */
    public function find(array $params = null, $findTags = true)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->photo->where($params)->first();

        $photo = $this->photo->fetchOne();

        if(empty($photo))
        {
            throw new TesisException(NO_RECORDS);
        }
        if($findTags == true)
        {
            $tags = $this->photoTagController->findAll(['photoId'=>$photo->id]);

            if(!empty($tags))
                $photo->tags = $tags;
        }


        return $photo ? $photo : false;
    }
    /**
     * findAll find all photos, append tags for each photo
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            $this->photo->select('*')->all();
        }
        else
        {
            $this->photo->where($params)->all();
        }

        $photos = $this->photo->fetch();

        if(!$photos)
        {
            throw new TesisException(NO_RECORDS);
        }

        foreach($photos as $k => $photo)
        {
            $tags = $this->photoTagController->findAll(['photoId'=>$photo->id]);
            if(!empty($tags))
                $photo->tags = $tags;
        }

        return $photos ? $photos : false;
    }
    /**
     * findByTag find photoId by tag and concatenate with photos
     *
     * @param int $id parameters can be empty or null
     *
     * @return object
     *
    */
    public function findByTag($id = '')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }
        $photoIds = $this->photoTagController->findAll(['tagId'=>$id]);

        if(!$photoIds) return false;

        $photos = [];

        if(sizeof($photoIds) > 0)
        {
            foreach($photoIds as $pid)
            {
                $param = ['id'=>$pid->photoId];
                $res = $this->find($param);
                if(!$res) return false;
                $photos[] = $res;
            }
        }

        return $photos ? $photos : false;
    }
    /**
     * findByLocation find photoId by tag and concatenate with photos
     *
     * @param float $latitude of the place selected
     * @param float $longitude of the place selected
     *
     * @return object
     *
    */
    public function findByLocation($latitude, $longitude, $distance)
    {
        if(empty($latitude) || empty($longitude) || empty($distance))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $query = 'SELECT id, name, latitude, longitude, ( 6371 * acos( cos( radians('.$latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$longitude.') ) + sin( radians('.$latitude.') ) * sin( radians( latitude ) ) ) ) AS distance FROM tesis_apis.photos HAVING distance < ' . $distance;

        $photo = $this->photo->exec($query);
        $photos = $photo->fetchAll();
        if(!$photos)
        {
            throw new TesisException(NO_RECORDS);
        }

        return $photos ? $photos : false;
    }
    /**
     * findByTagName find photoId by tag and concatenate with photos
     *
     * @param str $name parameters can be empty or null
     *
     * @return object
     *
    */
    public function findByTagName($name = '')
    {
        if(empty($name))
        {
            return false;
        }
        $photoIds = $this->photoTagController->findByTagName(['name'=>$name]);

        if(!$photoIds) return false;

        $photos = [];

        if(sizeof($photoIds) > 0)
        {
            foreach($photoIds as $pid)
            {
                $param = ['id'=>$pid->photoId];
                $res = $this->find($param);
                if(!$res) return false;
                $photos[] = $res;
            }
        }

        return $photos ? $photos : false;
    }
    /**
     * create
     *
     * @param array $params parameters needed to create new photo
     *
     * @return int lastInsertId
     *
    */
    public function create(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $create = $this->photo->create($params);
        return $create;
    }
    /**
     * update
     *
     * @param array $params parameters we'd like to update
     *              id is required
     *
     * @return object/bool
     *
    */
    public function update(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $update = $this->photo->update($params);

        return $update;
    }
    /**
     * delete
     *
     * @param int $id id of the photo
     *
     * @return object/bool
     *
    */
    public function delete($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $delete = $this->photo->delete($id);

        return $delete ? $delete : false;
    }
    /**
     * exists check if photo exists
     *
     * @param int $id id of the photo
     *
     * @return object/bool
     *
    */
    public function exists($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->photo->where(['id'=>$id])->first();

        $result = $this->photo->fetch();

        return !empty($result) ? $result : false;
    }
}
