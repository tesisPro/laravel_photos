<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\ProcessSource;
use Tesis\Photos\Core\Helpers\TesisException;

class ProcessSourceController
{
    /**
     * __construct initialize processSources
     *
     *
    */
    public function __construct()
    {
        $this->processSource = new ProcessSource;
        //$this->processController = new processController;
    }
    /**
     * exists check if source exists
     *
     * @param int $processId id of the process
     *
     * @return object/bool
     *
    */
    public function exists($processId='')
    {
        if(empty($processId))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->processSource->where(['processId'=>$processId])->all();
        $processSource = $this->processSource->fetch();

        return !empty($processSource) ? $processSource : false;
    }
    /**
     * action create/delete sources from processSources when the
     *        project is updated/deleted
     *
     * @param int    $processId id of the process newly created
     * @param array  $params    parameters can be empty or null
     * @param string $action    action required
     *
     * @return object
     *
    */
    public function action(array $sources=null, $processId, $action='create')
    {
        if(is_null($sources))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }
        foreach($sources as $sourceId)
        {
            $data = ['processId' => $processId, 'sourceId'=> $sourceId];
            if($action == 'create')
            {
                $updated = $this->processSource->create($data);
            }
            if($action == 'delete')
            {
                $updated = $this->processSource->delete($data);
            }
        }
    }

    public function create($processId, array $sources = null)
    {
        //check if sources not null, processId not null checked before calling this method
        if(empty($processId))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        //2-check if record exists in processSources
        $processSource = $this->exists($processId);
        if(empty($processSource))
        {
            //safe to insert
            $insert = $this->action($sources, $processId,'create');
        }
    }
    public function update($processId, array $sources = null)
    {
        $processSource = $this->exists($processId);

        //-- when editing process
        if(!empty($processSource))
        {
            $sourcesExist = []; //build sources existed
            $sourcesDelete = []; //build sources to delete
            foreach($processSource as $key=>$value)
            {
                array_push($sourcesExist, $value->sourceId);
            }
            //compare arrays, take further actions: insert/update
            $insert = array_diff($sources, $sourcesExist);
            $delete = array_diff($sourcesExist, $sources);

            if(!empty($insert))
            {
                //insert record
                $insert = $this->action($insert, $processId,'create');
            }
            if(!empty($delete))
            {
                //delete record
                $delete = $this->action($delete, $processId,'delete');
            }
        }
        //when inserting new process
        else
        {
            //safe to insert
            $insert = $this->action($sources, $processId,'create');
        }
    }
    public function delete($processId='')
    {
        //2-check if record exists in processSources
        $processSource = $this->exists($processId);
        if(!$processSource)
        {
            //throw new TesisException(NO_RECORDS);
            //nothing to delete from sources table
        }
        if(!empty($processSource))
        {
            $delete = []; //build sources to delete, ie: all occurences
            foreach($processSource as $key=>$value)
            {
                array_push($delete, $value->sourceId);
            }
            if(!empty($delete))
                $this->action($delete, $processId,'delete');
        }

    }
    /**
     * findAll find all sources for particular process
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            $this->processSource->select('*')->all();
        }
        else
        {
            $this->processSource->where($params)->all();
        }

        $sources = $this->processSource->fetch();

        return $sources ? $sources : false;
    }

}
