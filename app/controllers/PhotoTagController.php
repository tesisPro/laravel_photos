<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\PhotoTag;
use Tesis\Photos\Core\Tables\Tag;
use Tesis\Photos\Core\Helpers\TesisException;

class PhotoTagController
{
    /**
     * __construct initialize photoTag
     *
     *
    */
    public function __construct()
    {
        $this->photoTag = new photoTag;
        $this->tag = new Tag;
    }
    /**
     * exists check if tags exists
     *
     * @param int $photoId id of the photo
     *
     * @return object/bool
     *
    */
    public function exists($photoId='')
    {
        if(empty($photoId))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->photoTag->where(['photoId'=>$photoId])->all();
        $photoTag = $this->photoTag->fetch();

        return !empty($photoTag) ? $photoTag : false;
    }
    /**
     * action create/delete tags from photoTag when the
     *        project is updated/deleted
     *
     * @param int    $photoId id of the photo newly created
     * @param array  $params    parameters can be empty or null
     * @param string $action    action required
     *
     * @return object
     *
    */
    public function action(array $tags=null, $photoId, $action='create')
    {
        if(is_null($tags))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }
        foreach($tags as $tagId)
        {
            $data = ['photoId' => $photoId, 'tagId'=> $tagId];
            if($action == 'create')
            {
                $updated = $this->photoTag->create($data);
            }
            if($action == 'delete')
            {
                $updated = $this->photoTag->delete($data);
            }
        }
    }

    public function create($photoId, array $tags = null)
    {
        //check if tags not null, photoId not null checked before calling
        //this method
        if(empty($photoId))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        //2-check if record exists in photoTags
        $photoTag = $this->exists($photoId);
        if(empty($photoTag))
        {
            //safe to insert
            $insert = $this->action($tags, $photoId,'create');
        }
    }
    public function update($photoId, array $tags = null)
    {
        $photoTag = $this->exists($photoId);

        //-- when editing photo
        if(!empty($photoTag))
        {
            $tagsExist = []; //build sources existed
            $tagsDelete = []; //build sources to delete
            foreach($photoTag as $key=>$value)
            {
                array_push($tagsExist, $value->sourceId);
            }
            //compare arrays, take further actions: insert/update
            $insert = array_diff($tags, $tagsExist);
            $delete = array_diff($tagsExist, $tags);

            if(!empty($insert))
            {
                //insert record
                $insert = $this->action($insert, $photoId,'create');
            }
            if(!empty($delete))
            {
                //delete record
                $delete = $this->action($delete, $photoId,'delete');
            }
        }
        //when inserting new photo
        else
        {
            //safe to insert
            $insert = $this->action($tags, $photoId,'create');
        }
    }
    public function delete($photoId='')
    {
        //2-check if record exists in photoTags
        $photoTag = $this->exists($photoId);
        if(!$photoTag)
        {
            //throw new TesisException(NO_RECORDS);
            //nothing to delete from sources table
        }
        if(!empty($photoTag))
        {
            $delete = []; //build sources to delete, ie: all occurences
            foreach($photoTag as $key=>$value)
            {
                array_push($delete, $value->sourceId);
            }
            if(!empty($delete))
                $this->action($delete, $photoId,'delete');
        }

    }
    /**
     * findAll find all sources for particular photo
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null, $findTags = true)
    {
        if(is_null($params) || empty($params))
        {
            $this->photoTag->select('*')->all();
        }
        else
        {
            $this->photoTag->where($params)->all();
        }

        $tags = $this->photoTag->fetch();
        if(!$tags) continue;
        if($findTags == true)
        {
            foreach($tags as $tag)
            {
                $param = ['id'=>$tag->tagId];
                $this->tag->where($param)->first();
                $found = $this->tag->fetch();
                if($found)
                {
                    $found = array_shift($found);
                    $tag->name = $found->name;
                }
            }
        }

        return $tags ? $tags : false;
    }
    /**
     * findByTagName find all sources for particular photo by tag name
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findByTagName(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            return false;
        }

        $this->tag->where($params)->first();
        $tag = $this->tag->fetch();
        if(!$tag) return false;

        $tag = array_shift($tag);
        $param = ['tagId'=>$tag->id];
        $tags = $this->findAll($param);

        return $tags ? $tags : false;
    }

}
