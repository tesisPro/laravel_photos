<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\Process;
use Tesis\Photos\Core\Controllers\ProcessSourceController;
use Tesis\Photos\Core\Faces\SearchInterface;
use Tesis\Photos\Core\Faces\CrudInterface;
use Tesis\Photos\Core\Helpers\TesisException;

class ProcessController implements SearchInterface, CrudInterface
{

    /**
     * __construct initialize process and processSources
     *
     *
    */
    public function __construct()
    {
        $this->process = new Process;

        $this->ProcessSourceController = new ProcessSourceController;
    }
    /**
     * find single process, append sources for this process
     *
     * @param array $params parameters can be empty or null
     * @param bool  $findSources if set to true, search sources as well
     *                           otherwise only projects
     *
     * @return object
     *
    */
    public function find(array $params = null, $findSources = true)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->process->where($params)->first();

        $process = $this->process->fetchOne();

        //find sources
        if(empty($process))
        {
            throw new TesisException(NO_RECORDS);
        }
        if($findSources == true)
        {
            $sources = $this->ProcessSourceController->findAll(['processId'=>$process->id]);

            if(!empty($sources))
                $process->sources = $sources;
        }


        return $process ? $process : false;
    }
    /**
     * findAll find all processes, append sources for each process
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            $this->process->select('*')->all();
        }
        else
        {
            $this->process->where($params)->all();
        }

        $processes = $this->process->fetch();

        if(!$processes)
        {
            throw new TesisException(NO_RECORDS);
        }

        foreach($processes as $k => $process)
        {
            $sources = $this->ProcessSourceController->findAll(['processId'=>$process->id]);
            if(!empty($sources))
                $process->sources = $sources;
        }

        return $processes ? $processes : false;
    }
    /**
     * create
     *
     * @param array $params parameters needed to create new process
     *
     * @return int lastInsertId
     *
    */
    public function create(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $create = $this->process->create($params);
        return $create;
    }
    /**
     * update
     *
     * @param array $params parameters we'd like to update
     *              id is required
     *
     * @return object/bool
     *
    */
    public function update(array $params = null)
    {
        if(is_null($params))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $update = $this->process->update($params);

        return $update;
    }
    /**
     * delete
     *
     * @param int $id id of the process
     *
     * @return object/bool
     *
    */
    public function delete($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $delete = $this->process->delete($id);

        return $delete ? $delete : false;
    }
    /**
     * exists check if process exists
     *
     * @param int $id id of the process
     *
     * @return object/bool
     *
    */
    public function exists($id='')
    {
        if(empty($id))
        {
            throw new TesisException(MISSING_ARGUMENTS);
        }

        $this->process->where(['id'=>$id])->first();

        $result = $this->process->fetch();

        return !empty($result) ? $result : false;
    }
}
