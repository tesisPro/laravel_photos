<?php
namespace Tesis\Photos\Core\Controllers;

use Tesis\Database;
use Tesis\Photos\Core\Tables\Source;
use Tesis\Photos\Core\Faces\SearchInterface;

class SourceController implements SearchInterface
{
    /**
     * __construct initialize source
     *
     *
    */
    public function __construct()
    {
        $this->source = new Source;
    }
    /**
     * find find particular source
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function find(array $params = null)
    {
        $this->source->where($params)->first();

        $result = $this->source->fetch();

        return $result ? $result : false;
    }
    /**
     * findAll find all sources
     *
     * @param array $params parameters can be empty or null
     *
     * @return object
     *
    */
    public function findAll(array $params = null)
    {
        if(is_null($params) || empty($params))
        {
            $this->source->select('*')->all();
        }
        else
        {
            $this->source->where($params)->all();
        }
        $result = $this->source->fetch();

        return $result ? $result : false;
    }

}
