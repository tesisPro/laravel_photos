<?php
namespace Tesis\Photos\Core\Traits;

trait EncryptionTrait
{
    function base64_url_encode($data)
    {
      return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64_url_decode($data)
    {
      return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
    public function encryptClass($privateKey='')
    {
        $res = $this->generateHash($this->generateData(), $privateKey);
        //echo __METHOD__. $res . EOL;
        return $res;
    }
    public function generateHash($data= '', $key= '')
    {
        $res = hash_hmac("sha256", $data, $key);
        //echo __METHOD__. $res . EOL;
        return $res;
    }
    //
    public function generateData()
    {
        $returnValue = rawurlencode(json_encode($this));
        //echo __METHOD__. $res . EOL;
        return($returnValue);
    }
    /**
     * signClass
     * 1.step
     * validation code: generated data from generalHash, validationDateTime, public+privateT
     * prepare variables
     * initialize class var public token
     * initialize class var private token
     *                      validationDataTime
     *                      validationCode with generateHash func
     * assigned to an instance of a member
    */
    public function signClass($publicToken='', $privateToken='')
    {
        $this->publicToken = $publicToken;
        $this->validationDateTime = time();
        $this->validationCode = $this->generateHash($this->publicToken.$this->validationDateTime, $privateToken);
        //echo EOL . $this->validationCode . EOL;
    }
    /**
     * buildParams
     *
     * @param string $publicKey public key assigned to a user
     * @param string $prvateKey private key assigned to a user
     *
     * @return string whole query appended to the endpoint
     *
     * both params unique for a user
     *
     * ?apiKey=tesi123private&data=%7B%22encryptionTimestamp%22%3A1427442902%7D&hash=
     *  539166eae378d690f79c3e
     *
     *  initialize encryptionTimestamp
     *  get hash with encryptClass
     *  get data with generateData
     *
     */
    public function buildParams($publicKey='', $privateKey='')
    {
        if(!isset($this->encryptionTimestamp)) $this->encryptionTimestamp = time();
        $params = "?apiKey=" . $publicKey
                       . "&data=" . $this->generateData()
                       . "&hash=" . $this->encryptClass($privateKey)
                       . "&time=" . $this->encryptionTimestamp;
        //echo __METHOD__. $params . EOL;
        return $params;
    }
}
