<?php
namespace Tesis\Photos\Core\Traits;

trait ObjectTrait
{
    public function createObject($dataArray)
    {
        //create an object
        foreach ($this->dbFields as $val)
        {
            $this->$val = '';
        }
        //assign values to the object
        if(!is_null($dataArray))
        {
            foreach ($dataArray as $key => $value)
            {
                if (in_array( $key, $this->dbFields))
                    $this->$key = $value;
            }
        }
    }
}
