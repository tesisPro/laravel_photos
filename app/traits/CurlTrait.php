<?php

namespace Tesis\Photos\Core\Traits;

trait CurlTrait
{
    /**
     * goCurl
     *
     * @param string $endpoint url we want to connect to
     * @param string $data data returned from buildParams,
     *                     ie: parameters to append to endpoint ?apiKey=...
     * @param bool $isPost if is post request, set to true, otherwise to false
     *
     * @return
     *
     * create an instace of a user exended from sendable
     * start with 1.step, 2.step, so the user will have all variables instanitated
     * send request with this data
     *
     */
    public function goCurl($url, $data, $isPost=true)
    {
        $ch = curl_init();
        if ($isPost)
        {
            if (!is_array($data) && substr($data, 0, 1) == "?")
            {
                $data = substr($data, 1);
            }

            $options = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT      => 'cUrl',
                CURLOPT_CONNECTTIMEOUT => 10,      // timeout on connect
                CURLOPT_TIMEOUT        => 400,    // timeout on response - after 400s
                CURLOPT_FRESH_CONNECT  => true,
                CURLOPT_VERBOSE        => true,
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $data
            ];
        }
        else
        {
            $options = [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT      => 'cUrl',
                CURLOPT_CONNECTTIMEOUT => 10,      // timeout on connect 0=wait forever
                CURLOPT_TIMEOUT        => 400,    // timeout on response - after 400s
                CURLOPT_FRESH_CONNECT  => true,
                CURLOPT_VERBOSE        => true,
                CURLOPT_URL            => $url . $data
            ];
        }
        curl_setopt_array( $ch, $options);

        $result = curl_exec($ch);
        if (empty($result)){
            $error = curl_error($ch);
            $errorNumber = curl_errno($ch);

            $exception = new \Exception('curl call error: '.$error, $errorNumber);
            throw $exception;
        }
        curl_close($ch);

        $result = json_decode($result, true);
        return $result;

    }
}
