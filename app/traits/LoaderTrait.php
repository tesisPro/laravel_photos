<?php
namespace Tesis\Photos\Core\Traits;

use Tesis\Photos\Core\Helpers\Loader;

trait LoaderTrait {

    public function loader($configFile='')
    {
        if(empty($configFile)){
            $configFile = 'config.ini';
        }

        $config = Loader::load(dirname(dirname(__FILE__)).'/config/const.php');

        $configIni = new Loader();
        $configGet = $configIni->loadIni($configFile);

        defined('DB_HOST') ? null : define ('DB_HOST', $configGet->get('DB_HOST'));
        defined('DB_USER') ? null : define ('DB_USER', $configGet->get('DB_USER'));
        defined('DB_PASS') ? null : define ('DB_PASS', $configGet->get('DB_PASS'));
        defined('DB_NAME') ? null : define ('DB_NAME', $configGet->get('DB_NAME'));
        defined('DB_CHARSET') ? null : define ('DB_CHARSET', 'utf8');

        $this->sourceUrl    = $configGet->get('sourceUrl');
        $this->loginUrl     = $configGet->get('loginUrl');
        $this->userEndpoint = $configGet->get('userEndpoint');
        $this->photosEndpoint = $configGet->get('photosEndpoint');
        $this->privateKey   = $configGet->get('privateKey');
        $this->publicKey    = $configGet->get('publicKey');
        $this->environment  = $configGet->get('Environment');
    }

}
